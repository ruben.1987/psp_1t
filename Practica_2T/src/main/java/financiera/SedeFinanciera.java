package financiera;

import com.services.Constantes;
import com.threads.FinancieraServerThread;
import com.threads.HiloServerObject;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SedeFinanciera extends JFrame {
    public static void main(String[] args) {
        SedeFinanciera sedeFinanciera = new SedeFinanciera();
    }

    public static final String TITLE = "FINANCIERA";
    public static int WIDTH = 500;
    public static int HEIGTH = WIDTH;
    private JPanel content_pane;
    private JTextField txtMessage;
    private JTextArea taChatArea;
    private JButton btnConnect;
    private JButton btnLogOff;
    private JButton btnSend;
    private JButton btnSendGroup;
    private JButton btnCalculo;
    private ServerSocket server = null;
    private Socket socket = null;
    private FinancieraServerThread financieraServer=null;
    private boolean endConection = false;

    public SedeFinanciera() {
        initComponents();
    }

    public void initComponents() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGTH);
        setTitle(TITLE);
        content_pane = new JPanel();
        content_pane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(content_pane);
        txtMessage = new JTextField(30);
        txtMessage.setLocation(100, 250);
        taChatArea = new JTextArea();
        taChatArea.setColumns(40);
        taChatArea.setRows(10);
        taChatArea.setLocation(50, 300);
        btnConnect = new JButton();
        btnConnect.setText("CONNECT");
        btnConnect.setLocation(150, 450);
        btnLogOff = new JButton();
        btnLogOff.setText("LOG OFF");
        btnLogOff.setLocation(150, 450);
        btnLogOff.setEnabled(false);
        btnSend = new JButton();
        btnSend.setText("ENVIAR");
        btnSend.setLocation(150, 450);
        btnSendGroup = new JButton();
        btnSend.setEnabled(false);
        btnSendGroup.setText("TODOS");
        btnSendGroup.setLocation(50, 500);
        btnSendGroup.setEnabled(false);
        btnCalculo = new JButton();
        btnCalculo.setText("CALCULO");
        content_pane.add(txtMessage);
        content_pane.add(taChatArea);
        content_pane.add(btnConnect);
        content_pane.add(btnLogOff);
        content_pane.add(btnSend);
        content_pane.add(btnSendGroup);
        content_pane.add(btnCalculo);
        setVisible(true);

        btnConnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                financieraServer = new FinancieraServerThread(btnCalculo);
                financieraServer.start();
                btnConnect.setEnabled(false);
                btnLogOff.setEnabled(true);
                btnSend.setEnabled(true);
                btnSendGroup.setEnabled(true);
            }
        });

        btnLogOff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                financieraServer.exit();
                btnConnect.setEnabled(true);
                btnLogOff.setEnabled(false);
                btnSend.setEnabled(false);
                btnSendGroup.setEnabled(false);
            }
        });
    }
}
