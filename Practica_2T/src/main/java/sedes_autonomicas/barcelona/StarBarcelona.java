package sedes_autonomicas.barcelona;

import com.sedes.Autonomicas;
import com.services.Constantes;

import javax.swing.*;
import java.awt.*;

public class StarBarcelona {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Autonomicas barcelona = new Autonomicas(Constantes.NAME_BARCELONA);
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    barcelona.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
