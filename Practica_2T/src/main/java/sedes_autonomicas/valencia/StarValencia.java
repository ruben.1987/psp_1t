package sedes_autonomicas.valencia;

import com.sedes.Autonomicas;
import com.services.Constantes;

import javax.swing.*;
import java.awt.*;

public class StarValencia {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Autonomicas valencia = new Autonomicas(Constantes.NAME_VALENCIA);
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    valencia.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
