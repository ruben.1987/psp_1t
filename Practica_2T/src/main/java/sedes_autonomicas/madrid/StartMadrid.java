package sedes_autonomicas.madrid;

import com.sedes.Autonomicas;
import com.services.Constantes;

import javax.swing.*;
import java.awt.*;

public class StartMadrid {
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                Autonomicas madrid = new Autonomicas(Constantes.NAME_MADRID);
                try {
                    UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                    madrid.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
