package central;

import com.remote.RemoteServer;
import com.threads.HiloMultiCastServer;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;

public class SedeCentral extends JFrame {
    public static void main(String[] args) {
        SedeCentral sedeCentral = new SedeCentral();
    }

    public static final String TITLE = "CENTRAL";
    public static int WIDTH = 500;
    public static int HEIGTH = WIDTH;
    private JPanel content_pane;
    private JTextField txtMessage;
    private JTextArea taChatArea;
    private JButton btnConnect;
    private JButton btnLogOff;
    private JButton btnSend;
    private RemoteServer remoteServer;
    private HiloMultiCastServer hiloMultiCastServer = null;

    public SedeCentral() {
        initComponents();
    }

    public void initComponents() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGTH);
        setTitle(TITLE);
        content_pane = new JPanel();
        content_pane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(content_pane);
        txtMessage = new JTextField(30);
        txtMessage.setLocation(100, 250);
        taChatArea = new JTextArea();
        taChatArea.setColumns(40);
        taChatArea.setRows(10);
        taChatArea.setLocation(50, 300);
        btnConnect = new JButton();
        btnConnect.setText("CONNECT");
        btnLogOff.setText("LOG OFF");
        btnLogOff.setEnabled(false);
        btnSend.setText("ENVIAR");
        btnSend.setEnabled(false);
        content_pane.add(txtMessage);
        content_pane.add(taChatArea);
        content_pane.add(btnConnect);
        content_pane.add(btnSend);
        content_pane.add(btnLogOff);
        setVisible(true);

        btnConnect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloMultiCastServer = new HiloMultiCastServer(txtMessage);
                hiloMultiCastServer.start();
                try {
                    remoteServer = new RemoteServer();
                    remoteServer.contar();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
                btnSend.setEnabled(true);
                btnLogOff.setEnabled(true);
            }
        });

        btnLogOff.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloMultiCastServer.exit();
                btnConnect.setEnabled(true);
                btnSend.setEnabled(false);
                btnLogOff.setEnabled(false);
            }
        });

        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloMultiCastServer.sendMessage();
            }
        });
    }
}
