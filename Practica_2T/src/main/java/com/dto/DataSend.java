package com.dto;

import lombok.*;

import java.io.Serializable;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.time.LocalDate;


@ToString
@Getter
@NoArgsConstructor
public class DataSend implements Serializable {
    private String name;
    private LocalDate date;
    private byte[] sales;
    private PrivateKey key;

    @Setter
    private PublicKey publicKey;

    @Builder
    public DataSend(String name, LocalDate date, byte[] sales,PublicKey publicKey) {
        this.name = name;
        this.date = date;
        this.sales = sales;
        this.publicKey = publicKey;
    }
}
