package com.sedes;

import com.threads.HiloCastSedes;
import com.threads.HiloMultiCastCentral;
import com.threads.HiloSocketObject;
import lombok.SneakyThrows;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.Socket;

public class Autonomicas extends JFrame {
    public static int WIDTH = 500;
    public static int HEIGTH = WIDTH;
    private JPanel content_pane;
    private JTextField txtMessage;
    private JTextArea taChatArea;
    private JButton btnSend;
    private JButton btnSendData;
    private JButton btnConnect;
    private JButton btnLogOff;
    private Socket socket = null;
    private HiloSocketObject hiloSocketObject = null;
    private HiloMultiCastCentral hiloMultiCastCentral = null;
    private HiloCastSedes hiloCastSedes = null;

    public Autonomicas(String name) {
        final String HEADER = name;
        final String NAME = ">>>" + name + ": ";
        final String NAME_SHORT = name.toUpperCase();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(0, 0, WIDTH, HEIGTH);
        setTitle(HEADER);
        content_pane = new JPanel();
        content_pane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(content_pane);
        txtMessage = new JTextField(15);
        txtMessage.setLocation(50, 50);
        taChatArea = new JTextArea();
        taChatArea.setColumns(40);
        taChatArea.setRows(5);
        taChatArea.setLocation(50, 200);
        btnConnect = new JButton();
        btnConnect.setText("CONNECT");
        btnConnect.setLocation(50, 500);
        btnLogOff = new JButton();
        btnLogOff.setText("LOG OFF");
        btnLogOff.setLocation(50, 500);
        btnSend = new JButton();
        btnSend.setText("ENVIAR");
        btnSend.setLocation(50, 450);
        btnSendData = new JButton();
        btnSendData.setText("FINANCIERA");
        btnSendData.setLocation(50, 500);
        btnConnect.setEnabled(true);
        btnLogOff.setEnabled(false);
        btnSend.setEnabled(false);
        btnSendData.setEnabled(false);
        content_pane.add(txtMessage);
        content_pane.add(taChatArea);
        content_pane.add(btnConnect);
        content_pane.add(btnLogOff);
        content_pane.add(btnSend);
        content_pane.add(btnSendData);

        btnConnect.addActionListener(new ActionListener() {
            @SneakyThrows
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloSocketObject = new HiloSocketObject(new Socket());
                hiloSocketObject.start();
                hiloMultiCastCentral = new HiloMultiCastCentral();
                hiloMultiCastCentral.start();
                hiloCastSedes = new HiloCastSedes(taChatArea);
                hiloCastSedes.start();
                if (!hiloSocketObject.isInterrupted()) {
                    btnConnect.setEnabled(false);
                    btnLogOff.setEnabled(true);
                    btnSend.setEnabled(true);
                    btnSendData.setEnabled(true);
                }
            }
        });

        btnLogOff.addActionListener(new ActionListener() {
            @SneakyThrows
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloSocketObject.exit();
                hiloCastSedes.exit();
                hiloMultiCastCentral.exit();
                btnConnect.setEnabled(true);
                btnLogOff.setEnabled(false);
                btnSend.setEnabled(false);
                btnSendData.setEnabled(false);
            }
        });

        btnSend.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloCastSedes.send(NAME + txtMessage.getText());
                txtMessage.setText(null);
            }
        });

        btnSendData.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                hiloSocketObject.cripData(NAME_SHORT);
            }
        });
    }
}
