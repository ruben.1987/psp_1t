package com.services;

public class Constantes {
    public static final int PORT_CAST_SEDES=15234;
    public static final int PORT_CAST_CENTRAL=22222;
    public static final int PORT_OBJECT_FINANCIERA=3500;
    public static final int PORT_REMOTE_FINANCIERA=5555;
    public static final String HOST_REMOTE_FINANCIERA = "localhost";
    public static final String GROUP_NAME_CENTRAL = "238.0.0.255";
    public static final String GROUP_SEDES = "235.0.0.255";
    public static final double BAREMO_CANTIDAD_DIARIA=50000;
    public static final String GOOD_RESULT="GOOD RESULT ";
    public static final String BAD_RESULT="BAD RESULT ";
    public static final String KEY_REMOTE_METHOD = "CALCULO";
    public static final String NO_SERVER_UP = "SERVER IS NOT UP";
    public static final String NAME_MADRID = "Madrid";
    public static final String NAME_VALENCIA = "Valencia";
    public static final String NAME_BARCELONA = "Barcelona";
    public static final String NAME_FINANCIERA = "Financiera";
}
