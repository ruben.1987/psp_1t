package com.remote;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IntfcRemoteServer extends Remote {
    public boolean beneficios(double received,double cantidad) throws RemoteException;
}
