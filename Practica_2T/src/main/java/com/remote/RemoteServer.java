package com.remote;

import com.services.Constantes;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RemoteServer extends UnicastRemoteObject implements IntfcRemoteServer {

    public RemoteServer() throws RemoteException {
    }

    public void contar() {
        try {

            Registry registry = LocateRegistry.createRegistry(Constantes.PORT_REMOTE_FINANCIERA);

            registry.rebind(Constantes.KEY_REMOTE_METHOD, new RemoteServer());

            System.out.println("Server Ready");

        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean beneficios(double received,double cantidad) throws RemoteException {
        if(received>cantidad)return true;
        else return false;
    }
}
