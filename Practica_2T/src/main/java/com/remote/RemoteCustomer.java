package com.remote;

import com.services.Constantes;

import javax.swing.*;
import java.rmi.ConnectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Arrays;

public class RemoteCustomer {

    public boolean calculoBeneficios(double beneficios, double cantidadEstipulada) {
        boolean result = false;
        try {
            Registry registry = LocateRegistry.getRegistry(Constantes.HOST_REMOTE_FINANCIERA, Constantes.PORT_REMOTE_FINANCIERA);

            System.out.println(Arrays.toString(registry.list()));

            IntfcRemoteServer remote_object = (IntfcRemoteServer) registry.lookup(Constantes.KEY_REMOTE_METHOD);

            result = remote_object.beneficios(beneficios, cantidadEstipulada);

        } catch (ConnectException e) {
            JOptionPane.showMessageDialog(null, Constantes.NO_SERVER_UP);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {

        }

        return result;
    }
}
