package com.threads;

import com.services.Constantes;

import javax.swing.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class HiloMultiCastCentral extends Thread{
    private static final String HEADER=">>>Central: ";
    private MulticastSocket multicastSocket = null;
    private InetAddress localGroup = null;
    private DatagramPacket datagramPacket = null;
    private boolean endConnect=false;

    public HiloMultiCastCentral() {

    }

    @Override
    public synchronized void run(){
        try {
            multicastSocket = new MulticastSocket(Constantes.PORT_CAST_CENTRAL);
            localGroup = InetAddress.getByName(Constantes.GROUP_NAME_CENTRAL);

            multicastSocket.joinGroup(localGroup);

            String text = "";
            byte[] buffer = new byte[1024];
            while (!endConnect) {
                datagramPacket = new DatagramPacket(buffer, buffer.length);
                multicastSocket.receive(datagramPacket);
                text=new String(datagramPacket.getData());
                System.out.println(text.trim());
                JOptionPane.showMessageDialog(null,HEADER+text.trim(),HEADER,1);
                buffer = new byte[1024];
            }
            multicastSocket.leaveGroup(localGroup);
            multicastSocket.close();
            System.out.println("Multicast group leaved");
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit(){
        endConnect=true;
    }
}
