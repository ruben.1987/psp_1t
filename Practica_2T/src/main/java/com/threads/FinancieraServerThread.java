package com.threads;

import com.services.Constantes;

import javax.swing.*;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class FinancieraServerThread extends Thread {
    private ServerSocket server = null;
    private Socket socket = null;
    private boolean endConection = false;
    private JButton btnCalculo;

    public FinancieraServerThread(JButton btnCalculo) {
        this.btnCalculo = btnCalculo;
    }

    public void run() {
        try {
            System.out.println("Iniciando servidor object..........");
            server = new ServerSocket(Constantes.PORT_OBJECT_FINANCIERA);
            while (!endConection) {
                socket = server.accept();
                JOptionPane.showMessageDialog(null,"Conexion entrante de: " + socket.getInetAddress().getHostAddress());
                HiloServerObject hiloServerObject = new HiloServerObject(socket, btnCalculo);
                hiloServerObject.start();
            }
            System.out.println("Saliendo del servidor");
        } catch (
                IOException e) {
            e.printStackTrace();
        } finally {
            try {
                server.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void exit(){
        endConection=true;
    }
}
