package com.threads;

import com.services.Constantes;

import javax.swing.*;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

public class HiloMultiCastServer extends Thread {
    private MulticastSocket multicastSocket;
    private InetAddress localGroup = null;
    private JTextField txtOutputMessage = null;
    private DatagramPacket datagramPacket = null;
    private String text = "";
    private boolean send,exit;

    public HiloMultiCastServer(JTextField txtOutputMessage) {
        this.txtOutputMessage = txtOutputMessage;
    }

    @Override
    public synchronized void run() {
        try {
            send = false;
            exit = false;
            multicastSocket = new MulticastSocket();
            localGroup = InetAddress.getByName(Constantes.GROUP_NAME_CENTRAL);

            while (!exit) {

            }
            multicastSocket.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage() {
        try {
            text = txtOutputMessage.getText();
            datagramPacket = new DatagramPacket(text.getBytes(), text.length(), localGroup, Constantes.PORT_CAST_CENTRAL);
            multicastSocket.send(datagramPacket);
            send = false;
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit(){
        exit=true;
    }
}
