package com.threads;

import com.dto.DataSend;
import com.services.Constantes;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.time.LocalDate;

public class HiloSocketObject extends Thread {
    public PublicKey publicKey = null;
    private ObjectOutputStream objectOutputStream = null;
    private ObjectInputStream objectInputStream = null;
    private Socket cliente;
    private DataSend dataSend;
    private boolean exit, key;

    public HiloSocketObject(Socket cliente) {
        this.cliente = cliente;
    }

    @Override
    public synchronized void run() {
        try {
            exit = false;
            key = false;
            cliente = new Socket(Constantes.HOST_REMOTE_FINANCIERA, Constantes.PORT_OBJECT_FINANCIERA);
            objectOutputStream = new ObjectOutputStream(cliente.getOutputStream());
            objectInputStream = new ObjectInputStream(cliente.getInputStream());
            while (!exit) {
                if (!key) {
                    keyReceived();
                }
            }
        } catch (ConnectException e) {
            JOptionPane.showMessageDialog(null, Constantes.NO_SERVER_UP);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            {
                try {
                    objectOutputStream.close();
                    objectInputStream.close();
                    cliente.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void keyReceived() {
        try {
            publicKey = (PublicKey) objectInputStream.readObject();
            key = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(DataSend dataSend) {
        try {
            objectOutputStream.writeObject(dataSend);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit() {
        exit = true;
    }

    public void cripData(String name) {
        try {
            Cipher cipher = Cipher.getInstance("RSA");

            cipher.init(Cipher.ENCRYPT_MODE, publicKey);

            byte[] cifrado = cipher.doFinal(String.valueOf(Math.floor(Math.random() * 20000)).getBytes());

            dataSend = DataSend.builder()
                    .name(name)
                    .date(LocalDate.now())
                    .sales(cifrado)
                    .build();

            sendMessage(dataSend);

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
    }
}
