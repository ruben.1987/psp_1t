package com.threads;

import com.services.Constantes;

import javax.swing.*;
import java.io.IOException;
import java.net.*;

public class HiloCastSedes extends Thread {
    private MulticastSocket multicastSocket = null;
    private DatagramPacket dgpReceived = null;
    private DatagramPacket dgpSend = null;
    private InetAddress localGroup = null;
    private byte[] buffer = null;
    private JTextArea txaInputMessage = null;
    private String text = "";
    private boolean endConnect = false;

    public HiloCastSedes(JTextArea txaInputMessage) {
        this.txaInputMessage = txaInputMessage;
    }

    @Override
    public synchronized void run() {
        buffer = new byte[1024];
        try {
            multicastSocket = new MulticastSocket(Constantes.PORT_CAST_SEDES);
            localGroup = InetAddress.getByName(Constantes.GROUP_SEDES);
            multicastSocket.joinGroup(localGroup);

            while (!endConnect) {
                dgpReceived = new DatagramPacket(buffer, buffer.length);
                multicastSocket.receive(dgpReceived);
                text = new String(dgpReceived.getData());
                if(!endConnect) txaInputMessage.append(text.trim() + "\n");
            }

            multicastSocket.leaveGroup(localGroup);

            multicastSocket.close();

        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (ConnectException e) {
            JOptionPane.showMessageDialog(null, Constantes.NO_SERVER_UP);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void send(String message) {
        try {
            dgpSend = new DatagramPacket(message.getBytes(), message.length(), localGroup, Constantes.PORT_CAST_SEDES);
            multicastSocket.send(dgpSend);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exit() {
        endConnect = true;
    }
}
