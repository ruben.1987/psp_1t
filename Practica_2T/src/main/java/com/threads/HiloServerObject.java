package com.threads;

import com.dto.DataSend;
import com.remote.IntfcRemoteServer;
import com.remote.RemoteCustomer;
import com.services.Constantes;
import org.json.JSONObject;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.*;
import java.util.ArrayList;
import java.util.List;

public class HiloServerObject extends Thread {
    private JButton btnCalculo;
    private ObjectOutputStream objectOutputStream = null;
    private ObjectInputStream objectInputStream = null;
    private RemoteCustomer remoteCustomer;
    private DataSend dataSend;
    private Socket cliente = null;
    private PublicKey publicKey = null;
    private PrivateKey privateKey = null;
    private List<Double> quantities = null;
    private String customer = "";

    public HiloServerObject(Socket cliente, JButton btnCalculo) {
        this.cliente = cliente;
        this.btnCalculo = btnCalculo;
    }

    @Override
    public synchronized void run() {

        remoteCustomer = new RemoteCustomer();

        quantities = new ArrayList<>();

        btnCalculo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                double total = quantities.stream().mapToDouble(d -> d).sum();

                boolean result = remoteCustomer.calculoBeneficios(total, Constantes.BAREMO_CANTIDAD_DIARIA);
                quantities.clear();

                if (result) JOptionPane.showMessageDialog(null, Constantes.GOOD_RESULT + customer);
                else JOptionPane.showMessageDialog(null, Constantes.BAD_RESULT + customer,Constantes.NAME_FINANCIERA,1);
            }
        });

        try {
            objectOutputStream = new ObjectOutputStream(cliente.getOutputStream());
            objectInputStream = new ObjectInputStream(cliente.getInputStream());
            createKey();
            objectOutputStream.writeObject(publicKey);
            while ((dataSend = (DataSend) objectInputStream.readObject()) != null) {
                customer = dataSend.getName();
                desCripData(dataSend.getSales());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                objectOutputStream.close();
                objectInputStream.close();
                cliente.close();
            } catch (EOFException e) {
                JOptionPane.showMessageDialog(null, customer + " IS LOG OFF");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void createKey() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");

            generator.initialize(2048);

            KeyPair keyRSA = generator.generateKeyPair();

            publicKey = keyRSA.getPublic();

            privateKey = keyRSA.getPrivate();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public void desCripData(byte[] array) {
        try {
            Cipher cipher = Cipher.getInstance("RSA");

            cipher.init(Cipher.DECRYPT_MODE, privateKey);

            byte[] data = cipher.doFinal(dataSend.getSales());

            String result = new String(data);

            double number = Double.parseDouble(result);

            quantities.add(number);

            writeFileData(dataSend, number);


        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException e) {
            e.printStackTrace();

        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        }
    }

    public void writeFileData(DataSend dataSend, double number) {
        JSONObject object = new JSONObject();
        JSONObject dataObject = new JSONObject();
        dataObject.put("name", dataSend.getName());
        dataObject.put("date", dataSend.getDate());
        dataObject.put("money", number);
        object.put("money_day", dataObject);
        File file = new File(customer + ".json");
        PrintWriter pw = null;
        try {
            pw = new PrintWriter(file);
            pw.print(object.toString());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            pw.close();
        }
    }
}
